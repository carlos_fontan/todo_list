// DEPENDENCIES
import React, { createContext, useContext, useState } from "react";
import { v4 as uuid } from 'uuid'

export const TaskContext = createContext();

export const useTask = () => {
    return useContext(TaskContext);
}

const TaskProvider = ({ children }) => {

    const [tasks, setTask] = useState([]);

    const createTask = (title, description) => {
        setTask([...tasks, { title, description, id: uuid() }])
    }

    const updateTask = (id, updatedTask) => {

        const updatedTasks = tasks.map(task => (task.id === id) ? { ...task, ...updatedTask } : task)
        setTask([...updatedTasks])
    }

    const deleteTask = (id) => {

        const newListTask = tasks.filter(task => task.id !== id)
        setTask(newListTask)
    }


    return (
        <TaskContext.Provider value={{ tasks, createTask, updateTask, deleteTask }}>
            {children}
        </TaskContext.Provider>);
};

export default TaskProvider;
