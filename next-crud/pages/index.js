// DEPENDENCIES
import { useTask } from "../context/taskContext";
// COMPONENTS
import Head from "next/head";
import Layout from "../components/Layout";
// ICONS
import { VscTrash } from 'react-icons/vsc'
import { useRouter } from "next/router";

export default function Home() {

  const { tasks, deleteTask } = useTask()
  const { push } = useRouter()

  return (
    <div>
      <Head>
        <title>Todo List</title>
        <meta name="description" content="App todo list CRUD" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <Layout>
          <div className="flex justify-center">
            {
              !tasks.length
                ? <h2 className="my-10">There are no Task!!</h2>
                : (
                  <div className="w-10/12">
                    {tasks.map(({ title, description, id }, index) =>
                      <div
                        key={id}
                        className="bg-gray-700 hover:bg-gray-600 cursor-pointer px-10 py-5 m-2 flex justify-start items-center"
                        onClick={() => push(`/edit/${id}`)}
                      >
                        <span className="text-5xl mr-5">{index}</span>

                        <div className="w-full">
                          <div className="flex justify-between">
                            <h2 className="font-bold">{title}</h2>
                            <button
                              className="bg-red-700 hover:bg-red-600 px-3 py-1 inline-flex items-center rounded-md"
                              onClick={e => {
                                e.stopPropagation()
                                deleteTask(id)
                              }}
                            >
                              <VscTrash className="mr-2" />
                              Delete
                            </button>
                          </div>

                          <p className="text-gray-300">{description}</p>
                          <span className="text-gray-400 text-xs">{id}</span>
                        </div>
                      </div>
                    )}
                  </div>)

            }
          </div>
        </Layout>
      </main>
    </div>
  );
}

