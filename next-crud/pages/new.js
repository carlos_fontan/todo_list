// DEPENDENCIES
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useTask } from "../context/taskContext";

// COMPONENTS
import Head from "next/head"
import Layout from "../components/Layout"

const TaskFormPage = () => {

    const [task, setTask] = useState({
        title: '',
        description: '',
    });

    const { createTask, updateTask, tasks } = useTask()

    const { push, query } = useRouter()

    const handleChange = e => {
        const { name, value } = e.target
        setTask({ ...task, [name]: value })
    }

    const handleSubmit = e => {
        e.preventDefault()

        if (query.id) {
            updateTask(query.id, task)
        } else {
            createTask(task.title, task.description)
        }

        // Redirect home
        // 1. show modal or taks added sucessced
        push("/")
    }

    useEffect(() => {
        if (query.id) {
            const taskFound = tasks.find(task => task.id === query.id)
            setTask({
                title: taskFound.title,
                description: taskFound.description,
            })
        }
    }, [query.id, tasks]);

    const titlePage = query.id ? "Update a Task" : "Add a Task"

    return (
        <Layout>
            <Head>
                <title>{titlePage}</title>
                <meta name="description" content="Add new task" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <div >
                <form
                    onSubmit={handleSubmit}
                    className="bg-gray-700 p-10 h-2/4"
                >
                    <h1 className="text-2xl my-5">{titlePage}</h1>
                    <input
                        value={task.title}
                        name="title"
                        onChange={handleChange}
                        type="text"
                        placeholder="Write a title"
                        className="bg-gray-800 focus:text-gray-100 focus:outline-none w-full py-3 px-4 mb-5"
                    />
                    <textarea
                        value={task.description}
                        onChange={handleChange}
                        name="description"
                        cols="30"
                        rows="10"
                        placeholder="Write a description"
                        className="bg-gray-800 focus:text-gray-100 focus:outline-none w-full py-3 px-4 mb-5"
                    />
                    <button
                        type="submit"
                        className="bg-green-500 hover:bg-green-400 px-4 py-2 rounded-md disabled:opacity-30"
                        disabled={!task.title}
                    >
                        Save
                    </button>
                </form>
            </div>
        </Layout>
    )
}

export default TaskFormPage