import Link from 'next/link'
import { AiOutlinePlus } from 'react-icons/ai'
import { useRouter } from 'next/router'
import { useTask } from '../context/taskContext'

const Layout = ({ children }) => {

    const router = useRouter()
    
    const {tasks} = useTask()

    return (
        <div className='bg-gray-900 h-screen text-white'>
            <header className='bg-gray-800 text-white flex items-center px-10 py-5'>
                <Link href="/">
                    <a>
                        <h1 className='font-black text-lg'>
                            Task App
                        </h1>
                    </a>
                </Link>

                <span className='ml-2 text-gray-400 font-bold text-xs'>
                    {`Tasks: ${tasks.length}`}
                </span>

                <div className='flex-grow text-right'>
                    <button
                        onClick={() => router.push('/new')}
                        className='text-sm bg-green-500 hover:bg-green-400 px-5 py-2 font-bold rounded-md inline-flex items-center'>
                        <AiOutlinePlus className='mr-2' />
                        Add Task
                    </button>
                </div>
            </header>
            <main className='px-16 py-10 flex'>
                {children}
            </main>

        </div>
    )
}

export default Layout